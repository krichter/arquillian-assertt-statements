package richtercloud.arquillian.assertt.statements.web;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author richter
 */
@Named
@ApplicationScoped
public class HelloWorld implements Serializable {
    private static final long serialVersionUID = 1L;

    public HelloWorld() {
    }

    public String hello() {
        assert false;
        return "Hello world!";
    }
}
